pipeline {
    agent any
    options {
        buildDiscarder(logRotator(daysToKeepStr: '15', numToKeepStr: '20'))
        timestamps()
    }
    stages {
        stage("Initial-Validation") {
            steps{
                script {
                    if(params.BRANCH_NAME ==~ /^[a-z]+\/[A-Z][A-Z0-9]+-[0-9]+(.*)$/ || params.BRANCH_NAME == "develop") {
                        echo "Correct branch $params.BRANCH_NAME"
                    }
                    else {
                        error "Wrong branch $params.BRANCH_NAME"
                    }
                }
            }
        }
    }
}