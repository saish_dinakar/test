#!/bin/sh
push()
{
	git push origin --all
}
featureid_check(){
	echo "Please give the Jira FeatureID for the branch to be created"
	read feature_id
	(echo $feature_id | grep "^[A-Za-z]*-[0-9]\+$") || { 
	echo "Error: Feature name should be of the format <JIRA_PROJECT_CODE>-<TICKET_NUMBER>."; 
	featureid_check ;}
}
release(){
	echo "Please select minor release or major release Enter(1 or 2)"
	echo "1. minor"
	echo "2. major"
	echo "3. exit"
	read release
	case $release in 
	"1"|"minor")
		minor_release
		exit 0 ;;
	"2"|"major")
		git checkout master
		git checkout -b "release/$feature_id-major"
		push
		exit 0 
		;;
	"3"|"exit")
		exit 1 ;;
	*)
		echo "You have not selected valid option"
		release
		exit 0
	esac
}
minor_release(){
	echo "Select source branch - Enter(1 or 2)"
	echo "1. master"
	echo "2. develop"
	echo "3. exit"
	read source_branch
	case $source_branch in 
	"1"|"master")
		git checkout master
		git checkout -b "release/$feature_id-minor"
		push
		exit 0 ;;
	"2"|"develop")
		git checkout develop
		git checkout -b "release/$feature_id-minor"
		push
		exit 0 ;;
	"3"|"exit")
		exit 1 ;;
	*)
		echo "You have not selected valid option"
		minor_release
		exit 0
	esac
}
feature(){
	echo "Please give the source branch - Enter(1 or 2)"
	echo "1. develop"
	echo "2. release"
	echo "3. exit"
	read feature_source_branch
	case $feature_source_branch in
	"1"|"develop")
		git checkout develop
		git checkout -b "feature/$feature_id"
		push
		exit 0 ;;
	"2"|"release")
		echo "Please give the exact release branch name from where the new beanch should be created"
		read release_branch_name
		git checkout $release_branch_name
		git checkout -b "feature/$feature_id"
		push
		exit 0 ;;
	"3"|"exit")
		exit 1 ;;
	*)
		echo "You have not given any source branch ID"
		feature
	esac
}
branching(){
	echo "Please select the type of branch to be created - Enter(1,2,3 or 4)"
	echo "1. develop"
	echo "2. release"
	echo "3. feature"
	echo "4. hotfix"
	echo "5. exit"
	read branch
	case $branch in
	"1"|"develop")
		git checkout -b develop master	
		push
		exit 0
		;;
	"2"|"release")
		featureid_check
		release 
		exit 0 
		;;
	"3"|"feature")
		featureid_check
		feature
		exit 0
		;;
	"4"|"hotfix")
		featureid_check
		git checkout master	
		git checkout -b "hotfix/$feature_id"
		push
		exit 0
		;;
	"5"|"exit")
		exit 1 ;;
	*)
		echo "You have not selected any option or selected wrong option"
		branching
	esac
}
branching